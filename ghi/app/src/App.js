import { Fragment } from 'react';
import { BrowserRouter, NavLink, Route, Routes, useNavigate } from 'react-router-dom';
import AttendeesList from './AttendeesList';
import ConferenceForm from './ConferenceForm';
import LocationForm from './LocationForm';
import AttendConferenceForm from './AttendConferenceForm';
import Nav from './Nav';
import MainPage from './MainPage';
import PresentationForm from './PresentationForm';

function App(props) {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes path="/ end" >
          <Route  index  element={<MainPage />} />
          {/* <LocationForm /> */}
          {/* <AttendeesList attendees={props.attendees}/> */}
          {/* <ConferenceForm /> */}
          {/* <AttendConferenceForm /> */}
          <Route path="conferences">
             <Route path="new" element={<ConferenceForm />} />
          </Route>
          <Route path="attendees">
            <Route index element={<AttendeesList />} />
            <Route path="new" element={<AttendConferenceForm />} />
          </Route>
          <Route path="locations">
            <Route path="new" element={<LocationForm />} />
          </Route>
          <Route path="presentations">
            <Route path="new" element={<PresentationForm />} />
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
