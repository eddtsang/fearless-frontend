import React, {useEffect, useState } from "react";

const initialData = {
    presenter_name:"",
    company_name:"",
    presenter_email:"",
    title:"",
    synopsis:"",
    conference:""
}

function PresentationForm() {
    const [conferences, setConferences] = useState([]);
    const [formData, setFormData] = useState(initialData);

    const fetchConferenceData = async () => {
        const conferenceUrl = "http://localhost:8000/api/conferences/";
        const conferenceResponse = await fetch(conferenceUrl);
        // console.log("***", conferenceResponse);
        if ( conferenceResponse.ok) {
            const ConferenceData = await conferenceResponse.json();
            setConferences(ConferenceData.conferences);
            // console.log("** conference **",ConferenceData);
        }
    }

    useEffect( () => {
        fetchConferenceData();
    }, []);

    const handleFormChange = (e) => {
        setFormData({
            ...formData,
            [e.target.name]:e.target.value
        })
    }

    const handleSubmit = async (event) => {
        event.preventDefault();

        const presentationUrl = `http://localhost:8000/api/conferences/${formData.conference}/presentations/`;

        const fetchConfig = {
            method: "post",
            body: JSON.stringify(formData),
            headers: {
                "Content-Type" : "application/json"
            }
        };

        const presentationResponse = await fetch(presentationUrl, fetchConfig);
        if (presentationResponse.ok) {
            const newPresentation = await presentationResponse.json();
            console.log(newPresentation);

            setFormData(initialData);
        }


    }

    return(
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new presentation</h1>
            <form onSubmit={handleSubmit} id="create-presentation-form">
              <div className="form-floating mb-3">
                <input onChange={handleFormChange} value={formData.presenter_name} placeholder="Presenter name" required type="text" name="presenter_name" id="presenter_name" className="form-control" />
                <label htmlFor="presenter_name">Presenter name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleFormChange} value={formData.presenter_email} placeholder="Presenter email" required type="email" name="presenter_email" id="presenter_email" className="form-control" />
                <label htmlFor="presenter_email">Presenter email</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleFormChange} value={formData.company_name} placeholder="Company name" type="text" name="company_name" id="company_name" className="form-control" />
                <label htmlFor="company_name">Company name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleFormChange} value={formData.title} placeholder="Title" required type="text" name="title" id="title" className="form-control" />
                <label htmlFor="title">Title</label>
              </div>
              <div className="mb-3">
                <label htmlFor="synopsis">Synopsis</label>
                <textarea onChange={handleFormChange} value={formData.synopsis} className="form-control" id="synopsis" rows="3" name="synopsis" ></textarea>
              </div>
              <div className="mb-3">
                <select onChange={handleFormChange} required name="conference" id="conference" className="form-select" value={formData.conference}>
                  <option value="">Choose a conference</option>
                  { conferences.map( conference => {
                    return (
                        <option key={conference.id} value={conference.id}>
                            {conference.name}
                        </option>
                    )
                  })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    )

}

export default PresentationForm;
