window.addEventListener('DOMContentLoaded', async () => {
    // grab conferences info from API GET
    const locationsUrl = 'http://localhost:8000/api/locations/';

    const locationsResponse = await fetch(locationsUrl);

    if ( locationsResponse.ok ) {
        const locationsData = await locationsResponse.json();
        console.log(locationsData);
        const selectLocation = document.getElementById('location');
        for ( const location of locationsData.locations ){
            const option = document.createElement('option');
            option.value = location.id;
            option.innerHTML = location.name;
            selectLocation.appendChild(option);
        }
    }

    const formTag = document.getElementById('create-conference-form');
    formTag.addEventListener('submit', async event => {
        event.preventDefault();

        const formData = new FormData(formTag);
        // console.log("formData --> ", formData);
        const json = JSON.stringify(Object.fromEntries(formData));
        // console.log("JSON --> ", json);

        const conferencesUrl = 'http://localhost:8000/api/conferences/';
        const fetchConfig = {
            method: "post",
            body: json,
            headers: {
                'Content-Type': 'application/json',
            },
        }
        // console.log("fetchConfig --> ", fetchConfig);
        const response = await fetch( conferencesUrl, fetchConfig );
        // console.log("response --> ", response);

        if ( response.ok ){
            formTag.reset();
            const newConference = await response.json();
            // console.log("newConference -->", newConference);
        }
    })

})
