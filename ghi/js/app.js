function createCard(name, description, pictureUrl, start, end, locationName) {
    return `
        <div class="col-4">
            <div class="card shadow mb-3 bg-body-tertiary rounded">
                <img src="${pictureUrl}" class="card-img-top">
                <div class="card-body">
                <h5 class="card-title">${name}</h5>
                <h6 class="card-subtitle mb-2 text-muted">${locationName}</h6>
                <p class="card-text">${description}</p>
                </div>
                <div class="card-footer text-muted">
                ${start} - ${end}
                </div>
            </div>
        </div>
    `;
  }


function alert(text) {
    return `
    <div class="alert alert-warning d-flex align-items-center" role="alert">
        <svg class="bi flex-shrink-0 me-2" width="24" height="24" role="img" aria-label="Warning:"><use xlink:href="#exclamation-triangle-fill"/></svg>
        <div>
            ${text}
        </div>
    </div>
    `
}

window.addEventListener('DOMContentLoaded', async () => {
    const url = 'http://localhost:8000/api/conferences/';
    try {
        const response = await fetch(url);
        if (!response.ok){
            const retrieveError = alert("Error when pulling information.");
            const h2UpComingConf = document.querySelector('h2');
            h2UpComingConf.innerHTML += retrieveError;

        } else {
            const data = await response.json();
            console.log(data);
            for (let conference of data.conferences) {
                const detailUrl = `http://localhost:8000${conference.href}`
                const detailReponse = await fetch(detailUrl);
                if (detailReponse.ok){
                    const details = await detailReponse.json();
                    const title = details.conference.name;
                    const description = details.conference.description;
                    const pictureUrl = details.conference.location.picture_url;
                    const start = new Date(details.conference.starts).toLocaleDateString("en-US");
                    const end = new Date(details.conference.ends).toLocaleDateString("en-US");
                    const locatinName = details.conference.location.name;
                    const html = createCard(title, description, pictureUrl, start, end, locatinName);
                    // console.log(html);
                    const column = document.querySelector('.row');
                    column.innerHTML += html;
                }
            }
        }
    } catch (e) {
        const processingError = alert("Error!");
        const h2UpComingConfError = document.querySelector('h2');
        h2UpComingConfError.innerHTML += retrieveError;
    }
});
