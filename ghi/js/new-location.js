window.addEventListener('DOMContentLoaded', async () => {
// grab states info from API GET
    const statesUrl = 'http://localhost:8000/api/states/';

    // as statesResponse is waiting to get info back, JS can run other codes
    const statesResponse = await fetch(statesUrl);

    // when statesReponse.ok is true
    if ( statesResponse.ok ) {
        const statesData = await statesResponse.json();
        const selectState = document.getElementById('state');
        for ( const state of statesData.states ) {
            const option = document.createElement('option');
            option.value = state.abbreviation;
            option.innerHTML = state.name;
            selectState.appendChild(option);
        }
    }

    const formTag = document.getElementById('create-location-form');
    formTag.addEventListener('submit', async event => {
        event.preventDefault();

        const formData = new FormData(formTag);
        const json = JSON.stringify(Object.fromEntries(formData));
        // console.log("json: -->", json);

        const locationsUrl = 'http://localhost:8000/api/locations/';
        const fetchConfig = {
            method: "post",
            body: json,
            headers : {
                'Content-Type' : 'application/json',
            },
        };
        // console.log("fetchConfig --> ", fetchConfig);
        const response = await fetch(locationsUrl, fetchConfig);
        // console.log("response --> ", response);
        if ( response.ok ) {
            formTag.reset();
            const newLocation = await response.json();
            // console.log("newLocation -->", newLocation);
        }
    })
})
